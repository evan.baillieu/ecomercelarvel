<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\PanierController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// User routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

// Produit routes
Route::get('/produits', [ProduitController::class, 'index']);
Route::get('/produits/{id}', [ProduitController::class, 'show']);
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/produits', [ProduitController::class, 'create']);
    // La j'ai mis un post parce que le put ne fonctionne pas avec l'image
    Route::post('/produits/{id}', [ProduitController::class, 'update']);
    Route::delete('/produits/{id}', [ProduitController::class, 'destroy']);
});

// Panier routes
Route::middleware('auth:sanctum')->group(function () {
    Route::get('/panier/view', [PanierController::class, 'viewCart']);
    Route::post('/panier/add', [PanierController::class, 'addToCart']);
    Route::patch('/panier/update/{id}', [PanierController::class, 'updateCart']);
    Route::delete('/panier/delete/{id}', [PanierController::class, 'deleteCartItem']);
});
