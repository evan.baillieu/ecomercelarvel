<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        $role = $request->role ?? 'client';

        if (!in_array($role, ['administrateur', 'client'])) {
            return response()->json(['error' => "Rôle invalide"], 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $role,
        ]);

        return response()->json([
            'message' => 'Utilisateur crée avec succès !',
            'user' => $user,
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        if (!auth()->attempt($credentials)) {
            return response()->json([
                'message' => 'Non autorisé',
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $plainTextToken = $tokenResult->plainTextToken;


        return response()->json([
            'access_token' => $plainTextToken,
            'token_type' => 'Bearer',
            'role' => $user->role,
        ]);
    }
}
