<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produit;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProduitController extends Controller
{
    public function index()
    {
        $produits = Produit::all();
        return response()->json($produits);
    }

    public function show($id)
    {
        $produit = Produit::find($id);
        if ($produit) {
            return response()->json($produit);
        } else {
            return response()->json([
                'message' => 'Produit non trouvé !',
            ], 404);
        }
    }

    public function create(Request $request)
    {
        $user = auth()->user();

        if ($user->role !== 'administrateur') {
            return response()->json([
                'message' => 'Vous n\'êtes pas autorisé à effectuer cette action !',
            ], 403);
        }

        $request->validate([
            'titre' => 'required|string',
            'description' => 'required|string',
            'prix' => 'required|numeric',
            'quantite' => 'required|integer',
            'image' => 'sometimes|image|max:2048',
        ]);

        if ($request->hasFile('image')) {

            $imagePath = $request->file('image')->store('produits', 'public');
        } else {
            return response()->json(['error' => "Problème de téléchargement de l'image"], 400);
        }

        $produit = Produit::create([
            'titre' => $request->titre,
            'description' => $request->description,
            'prix' => $request->prix,
            'quantite' => $request->quantite,
            'image' => $imagePath,
        ]);

        return response()->json([
            'message' => 'Produit créé avec succès !',
            'produit' => $produit,
        ], 201);
    }



    public function update(Request $request, $id)
    {
        $user = auth()->user();

        if ($user->role !== 'administrateur') {
            return response()->json([
                'message' => 'Vous n\'êtes pas autorisé à effectuer cette action !',
            ], 403);
        }
        $produit = Produit::find($id);
        if ($produit) {
            $request->validate([
                'titre' => 'required|string',
                'description' => 'required|string',
                'prix' => 'required|numeric',
                'quantite' => 'required|integer',
                'image' => 'sometimes|image|max:2048',
            ]);

            $produit->titre = $request->titre;
            $produit->description = $request->description;
            $produit->prix = $request->prix;
            $produit->quantite = $request->quantite;

            if ($request->hasFile('image')) {
                if ($produit->image) {
                    Storage::delete('public/' . $produit->image);
                }

                $imagePath = $request->file('image')->store('produits', 'public');
                $produit->image = $imagePath;
            }

            $produit->save();

            return response()->json([
                'message' => 'Produit modifié avec succès !',
                'produit' => $produit,
            ]);
        } else {
            return response()->json([
                'message' => 'Produit non trouvé !',
            ], 404);
        }
    }

    public function destroy($id)
    {
        $user = auth()->user();

        if ($user->role !== 'administrateur') {
            return response()->json([
                'message' => 'Vous n\'êtes pas autorisé à effectuer cette action !',
            ], 403);
        }
        $produit = Produit::find($id);
        if ($produit) {
            if ($produit->image) {
                Storage::delete('public/' . $produit->image);
            }

            $produit->delete();

            return response()->json([
                'message' => 'Produit supprimé avec succès !',
                "produit" => $produit,
            ]);
        } else {
            return response()->json([
                'message' => 'Produit non trouvé !',
            ], 404);
        }
    }
}
