<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Panier;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PanierController extends Controller
{

    public function viewCart()
    {
        $user = auth()->user();

        if (!$user) {
            return response()->json(['message' => 'Utilisateur non authentifié'], 401);
        }

        $panierItems = Panier::with('produit')->where('user_id', $user->id)->get();

        if ($panierItems->isEmpty()) {
            return response()->json(['message' => 'Votre panier est vide'], 200);
        }

        return response()->json([
            'message' => 'Contenu du panier récupéré avec succès',
            'panier' => $panierItems
        ], 200);
    }


    public function addToCart(Request $request)
    {
        try {
            $user = Auth::user();

            $validator = Validator::make($request->all(), [
                'produit_id' => 'required|exists:produits,id',
                'quantite' => 'required|integer|min:1'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $panier = Panier::where('user_id', $user->id)
                ->where('produit_id', $request->produit_id)
                ->first();

            if ($panier) {
                $panier->quantite += $request->quantite;
                $panier->save();
            } else {
                $panier = Panier::create([
                    'user_id' => $user->id,
                    'produit_id' => $request->produit_id,
                    'quantite' => $request->quantite
                ]);
            }

            return response()->json([
                'message' => 'Produit ajouté au panier avec succès!',
                'Panier' => $panier
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => "Une erreur s'est produite lors de l'ajout du produit au panier.",
                'details' => $e->getMessage()
            ], 500);
        }
    }

    public function updateCart(Request $request, $id)
    {
        $user = auth()->user();
        $validator = Validator::make($request->all(), [
            'quantite' => 'required|integer|min:1'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $panierItem = Panier::where('user_id', $user->id)
            ->where('id', $id)
            ->first();

        if (!$panierItem) {
            return response()->json(['message' => 'Article non trouvé dans le panier.'], 404);
        }

        $panierItem->quantite = $request->quantite;
        $panierItem->save();

        return response()->json(['message' => 'Panier mis à jour avec succès.', 'panier' => $panierItem], 200);
    }

    public function deleteCartItem($id)
    {
        $user = auth()->user();
        $panierItem = Panier::where('user_id', $user->id)
            ->where('id', $id)
            ->first();

        if (!$panierItem) {
            return response()->json(['message' => 'Article non trouvé dans le panier.'], 404);
        }

        $panierItem->delete();

        return response()->json(['message' => 'Article supprimé du panier avec succès.'], 200);
    }
}
